defmodule LearnUWeb.Router do
  use LearnUWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LearnUWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    get "/register", Accounts.RegistrationController, :new
    post "/register", Accounts.RegistrationController, :create

    get "/login", SessionController, :new
    post "/login", SessionController, :create

    delete "/logout", SessionController, :delete

    resources "/accounts/password", Accounts.PasswordController,
      only: [:new, :create, :edit, :update],
      singleton: true
  end

  # Other scopes may use custom stacks.
  # scope "/api", LearnUWeb do
  #   pipe_through :api
  # end
end
