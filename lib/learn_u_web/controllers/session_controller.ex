defmodule LearnUWeb.SessionController do
  use LearnUWeb, :controller

  plug :scrub_params, "session" when action in [:create]

  def new(conn, _params) do
    render(conn, "new.html")
  end

  @spec create(Plug.Conn.t, map) :: Plug.Conn.t
  def create(conn, %{"session" => params}) do
    render(conn, "new.html")
  end

  @spec delete(Plug.Conn.t, map) :: Plug.Conn.t
  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You've been successfully logged out.")
    |> redirect(to: session_path(conn, :new))
  end
end
