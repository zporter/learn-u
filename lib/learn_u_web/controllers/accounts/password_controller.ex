defmodule LearnUWeb.Accounts.PasswordController do
  use LearnUWeb, :controller

  plug :scrub_params, "account" when action in [:create, :update]

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"account" => %{"email" => email}}) do
    # Send instructions via email
    conn
    |> put_flash(:info, "You will receive password reset instructions momentarily.")
    |> render("new.html")
  end

  def edit(conn, %{"t" => token}) do
    render(conn, "edit.html")
  end

  def update(conn, %{"account" => params}) do
    # Update the account password. Handle success and failure.
    conn
    |> put_flash(:info, "Password successfully reset. Please try logging in again.")
    |> redirect(to: session_path(conn, :new))
  end
end
