defmodule LearnUWeb.Accounts.RegistrationController do
  use LearnUWeb, :controller

  alias LearnU.Accounts

  plug :scrub_params, "registration" when action in [:create]

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"registration" => params}) do
    render conn, "new.html"
  end
end
