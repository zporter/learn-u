defmodule LearnUWeb.PageController do
  use LearnUWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
