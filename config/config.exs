# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :learn_u,
  ecto_repos: [LearnU.Repo]

# Configures the endpoint
config :learn_u, LearnUWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "7dLLtz2zGMzjxo5IRKSeKJEG+L3PDM6KzxLM8/13miOehG8lw+Qvr7xz9vEFFlZS",
  render_errors: [view: LearnUWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: LearnU.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
